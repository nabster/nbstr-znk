/*
                                                                   
                     ##########################                    
                  ########               ########                  
                #####                         #####                
               ###                               ###               
             ###                                  ###              
            ###                                     ##             
            ##                                       ##            
           ###                                    #  ##            
           ##  ##                                 ##  ##           
           ##  ##                                 ##  ##           
           ##  ##                                 ##  ##           
           ### ###                               ### ###           
            ##  ##                               ##  ##            
            ### ##     #######       #######     ## ###            
             ## ##  ###########     ###########  #####             
              #### ############     ############ ####              
               ### ###########       ########### ###         #     
   #####       ###  #########         #########  ###      #####    
   ## ####     ##    #######    # #    #######    ##     ###  ##   
   ##   ###    ##      ###    ### ##     ###     ###    ###   ##   
 ####    #####  ##            ### ###            ##  #####    #### 
###        ##########        #### ####       ##########         ###
#########      ##########    #### ####    ##########      #########
 ############      #######    ##   ##    #######      ############ 
          ######    #### ##             ## ####   #######          
             ########### ##################### ######              
                 ########### ### # ###### # ######                 
                    ##  ################### ###                    
                 ###### #### ### # ######## ######                 
             ######  ##   ###############   ## #######             
    ############     ###                   ###     ############    
    ##   ###      ########               ########      ###  ###    
    ###        ######   ######      #######   ######       ####    
      ###    #####        ###############        #####    ###      
       ##   ###                                     ###   ##       
       ######                                        #######       
        ####                                           ####        
                                                                   
*/
var moment = require('moment'),
    START_TIME = moment();

var fs = require('fs');
var colors = require('colors');
var _ = require('underscore');

// PROTOTYPE
String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

function success() {
    var duration = moment().diff(START_TIME, 'milliseconds')
    console.log(('\nsuccess : script completed in ' + duration/1000 + ' s.\n').green);
}

function readCode(fn) {

    fs.createReadStream('code.txt').on('data', function(data) {
        fn(data.toString('utf8'));
    });
}

function substitute (_str, sub) {
	var new_str = _str;
	for(var l in _str){
		if(sub[_str[l]]){
			new_str = new_str.replaceAt(parseInt(l),sub[_str[l]]);
		}		
	}
	// console.log(new_str);
	return new_str
}

// LOAD FILES AND SET CONSTANTS
function readLines(input, func, callback) {
    var remaining = '',
        input = fs.createReadStream(input);

    input.on('data', function(data) {
        remaining += data;
        var index = remaining.indexOf('\n');
        var last = 0;
        while (index > -1) {
            var line = remaining.substring(last, index);
            last = index + 1;
            func(line);
            index = remaining.indexOf('\n', last);
        }

        remaining = remaining.substring(last);
    });

    input.on('end', function() {
        if (remaining.length > 0) {
            func(remaining);
        }
        if (typeof(callback) === 'function') {
            callback();
        }
    });
}
var C = {
    dict_2: [],
    dict_3: [],
    dict_4: []
};
var CNST = {
    letters: 'abcdefghijklmnopqrstuvwxyz'.split(''),
    substitution: {}
};
var codex = {};

for (var D in C) {
    readLines('dict/' + D + '.txt', function(word) {
        C[D].push(word);
    }, function() {
        // console.log(C[D]);
    });
}

// FIRST ROUND : 2 LETTERS
readCode(function(data) {
    var code_2 = [],
        data_2 = data;

    data_2=data_2.replace(/[,\.\']/g, ' ').replace(/[\s]+/g, ' ').split(' ');

    for (var W in data_2) {
        if (data_2[W].length == 2) {
            code_2.push(data_2[W]);
        }
    }

    // console.log(code_2);

    for(var w in code_2){
    	var word=code_2[w];
    	console.log(word);
    	for(var l=0;l<word.length;l++){
    		var letter = word.charAt(l);
    	}
    }

    success();

});
